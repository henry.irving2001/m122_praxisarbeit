cwd=$(pwd)
cd $(dirname $0)
BINDIR=$(pwd)
cd $cwd
BASENAME=$(basename 0$)
TMPDIR=/tmp/$BAENAME.$$

source ../etc/backupCreate.env

touch users.txt;



cat $ETCDIR/groupnames.env|grep -v '^$'|grep -v '^#'|while read groupname 
do
  if [ $(getent group $groupname) ]; then
    gid="$(getent group "$groupname" | cut -d: -f4)"
    echo "$gid" >> users.txt;
  else
    echo "group $groupname does not exist."
  fi
done



sed -i 's/,/\n/g' users.txt;


awk '!seen[$0]++' users.txt > userList.txt;


rm -r users.txt;

cat userList.txt|grep -v '^$'|grep -v '^#'|while read user
do 

  if [ -d "/home/$user" ]; then

    awk -F: -v username=$user '$1==username {print $6}' /etc/passwd >> directoryList.txt
  else
    echo "no /home/$user directory";
  fi
done

filename=$NAME-$(date +%d-%m-%Y)

tar -cvf backup/$filename.tar -T directoryList.txt

find $TARGET_DIRECTORY -type f -mtime +$DATE_TILL_DELETE -name '*.tar' -execdir rm -- '{}' \;
